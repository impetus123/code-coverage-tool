import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

public class GetOverallCoverage {

	static Connection conn = null;
	static PreparedStatement pst = null;
	static ResultSet rs = null;
	static LogEntry logEntry = new LogEntry();
	static String query = "select * from coverage.svn_log where message like ? ";
	static float totalLineCount = 0, i = 0;

	public static void main(String[] args) throws SQLException, IOException {

		LogEntry logEntry = new LogEntry();
		conn = logEntry.connect();

		pst = conn.prepareStatement(query);
		pst.setString(1, "US-1234%");

		// pst.setString(1, args[0]);
		rs = pst.executeQuery();
		String revision = new String();
		int index = 0, coverage = 0;
		float total = 0f, tot = 0f;

		while (rs.next()) {
			
			revision = rs.getString("REVISION");
			tot = getBlame(revision);
			total = total + tot;
			// if(tot!=0f)
			index++;

		}
		if (index == 0) {
			index = 1;
		}
		coverage = (int) total / index;

		System.out.println("Overall Coverage : " + coverage);

	}

	static float getBlame(String revison) throws SQLException {

		conn = logEntry.connect();
		ResultSet rs = null;
		pst = conn
				.prepareStatement("select * from file_blame where revision=? ");
		String fileName = new String();
		String lineNumber = new String();
		pst.setString(1, revison);
		rs = pst.executeQuery();
		int count = 0;
		float coverageData = 0, total = 0, tot = 0f;
		while (rs.next()) {
			fileName = rs.getString("file_name");
			lineNumber = rs.getString("line_number");
			if (fileName != null & lineNumber != null) {
				getCovergae(fileName, lineNumber);
			}

		}
		if (i == 0) {
			i = 1;
		}

		total = (float) (totalLineCount / i) * 100;

		return total;

	}

	static void getCovergae(String fileName, String lineNumber)
			throws SQLException {

		List<Coverage> coverageList = new ArrayList<Coverage>();
		Coverage currCoverage = null;
		String tagContent = null;
		FileReader fr = null;
		Coverage coverageInn = new Coverage();
		Connection conn = coverageInn.connect();
		PreparedStatement pst = null;
		int hits = 0, totalHits = 0;

		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
			fr = new FileReader("./xml/coverage/coverage.xml");
			XMLStreamReader reader = factory.createXMLStreamReader(fr);

			String currClass = new String();
			String currMethod = new String();

			pst = conn
					.prepareStatement("insert into coverage_details (CLASS_NAME,METHOD,LINE_NUMBER,HITS) values (?,?,?,?)");

			while (reader.hasNext()) {
				int event = reader.next();

				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if ("class".equals(reader.getLocalName())) {
						currClass = reader.getAttributeValue(0);
					}
					if ("method".equals(reader.getLocalName())) {
						currMethod = reader.getAttributeValue(0);
					}
					if ("line".equals(reader.getLocalName())) {
						currCoverage = new Coverage();
						currCoverage.setClassName(currClass);
						currCoverage.setMethod(currMethod);
						currCoverage.setLineNumber(reader.getAttributeValue(0));
						currCoverage.setHits(Integer.parseInt(reader
								.getAttributeValue(1)));
						coverageList.add(currCoverage);
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					tagContent = reader.getText().trim();
					break;

				case XMLStreamConstants.END_ELEMENT:
					switch (reader.getLocalName()) {
					case "class":
						currClass = "";
						break;
					case "method":
						currMethod = "";
						break;
					}
					break;

				case XMLStreamConstants.START_DOCUMENT:
					coverageList = new ArrayList<Coverage>();
					break;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null)
					fr.close();
			} catch (Exception p) {

			}
		}

		for (Coverage coverage : coverageList) {

			try {

				fileName = fileName.replace("src\\main\\java\\", "").replace(
						"\\", ".");
				if (fileName.contains(coverage.getClassName() + ".java")) {
					if (lineNumber.equals(coverage.getLineNumber())) {
						hits = coverage.getHits();
						if (hits > 0) {
							totalLineCount++;
						}
						i++;
					}

				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		// System.out.println(fileName+"::"+lineNumber+":::"+":::::"+i+"::::"+totalLineCount+":::::");
		if (i == 0) {
			i = 1;
		}

		// return ((totalLineCount / i) * 100);

	}
}
