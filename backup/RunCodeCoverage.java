import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.stream.XMLStreamException;

import com.mysql.jdbc.Connection;

public class RunCodeCoverage {

	static final String selectQuery = "select distinct(path) from svn_log where message like ?";
	static java.sql.Connection conn = null;
	public static File folder = new File("./xml/");
	static String tempFileName = "";

	public static void main(String args[]) throws XMLStreamException,
			IOException, SQLException {
		conn = new LogEntry().connect();

		System.out.println("Going to get svn logs and persist into DB");
		createSVNLog();

		System.out.println("Going to get svn Blamed files and persist into DB");
		createSvnFileBlame();

		System.out.println("Going to get Coverage");
		GetOverallCoverage.main(args);

	}

	static void createSVNLog() {

		String s = null, fileName = "./xml/log/svnLog.xml";

		try {

			Process p = Runtime
					.getRuntime()
					.exec("svn log --xml -r BASE:HEAD --verbose D:\\DNBI_Branches\\a3\\");

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			File file = new File(fileName);
			FileOutputStream is = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(is);
			Writer w = new BufferedWriter(osw);
			while ((s = stdInput.readLine()) != null) {
				w.write(s + "\n");
			}
			w.close();
			LogStax.saveLogInDB(fileName);
			System.out.println("Logs Saved Successfully.....\n\n");
		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}

	}

	static void createSvnFileBlame() throws SQLException {

		String s = null;
		LogEntry logEntry = new LogEntry();

		PreparedStatement pst = null;
		ResultSet rs = null;
		pst = conn.prepareStatement(selectQuery);
		pst.setString(1, "US-1234%");
		rs = pst.executeQuery();
		String fileName = new String();
		String postFix = new String();
		String fileToDB = new String();
		while (rs.next()) {

			fileName = rs.getString("PATH").replace("[", "").replace("]", "");
			postFix = fileName.substring(fileName.lastIndexOf("/") + 1,
					fileName.indexOf("."));
			try {

				Process p = Runtime.getRuntime().exec(
						"svn blame --xml D:\\DNBI_Branches\\a3\\" + fileName);

				BufferedReader stdInput = new BufferedReader(
						new InputStreamReader(p.getInputStream()));

				BufferedReader stdError = new BufferedReader(
						new InputStreamReader(p.getErrorStream()));
				File file = new File("./xml/blame/svnBlame_" + postFix + ".xml");
				fileToDB = "./xml/blame/svnBlame_" + postFix + ".xml";
				FileOutputStream is = new FileOutputStream(file);
				OutputStreamWriter osw = new OutputStreamWriter(is);
				Writer w = new BufferedWriter(osw);
				while ((s = stdInput.readLine()) != null) {
					w.write(s + "\n");
				}
				w.close();
				BlameStax.persistBlame(fileToDB);
			} catch (IOException e) {
				System.out.println("exception happened - here's what I know: "
						+ e);
				e.printStackTrace();
				System.exit(-1);
			}

		}
		System.out.println("Logs Saved Successfully.....\n\n");

	}

	/*
	 * public static void listFilesForFolder(final File folder) {
	 * 
	 * for (final File fileEntry : folder.listFiles()) { if
	 * (fileEntry.isDirectory()) { //
	 * System.out.println("New folder Name:\n \n"+fileEntry);
	 * listFilesForFolder(fileEntry); } else { if (fileEntry.isFile()) {
	 * tempFileName = folder.getPath() + "\\" + fileEntry.getName();
	 * 
	 * // System.out.println("File= " + folder.getPath()+ "\\" + //
	 * fileEntry.getName());
	 * 
	 * if (".\\xml\\blame".equalsIgnoreCase(folder.getPath())) { //
	 * System.out.println("HSTC:  "+folder.getPath());
	 * BlameStax.persistBlame(tempFileName); }
	 * 
	 * // BlameStax.persistBalm(); }
	 * 
	 * } } }
	 */

}