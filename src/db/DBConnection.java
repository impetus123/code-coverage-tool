package db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	
	public static Connection connect() {
		String url = "jdbc:mysql://localhost:3306/coverage";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "root";
		String password = "root";
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}