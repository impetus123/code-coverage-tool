class GitLogInfo {

	private String author;
	private String revision;
	private String message;
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	/*
	 * @Override public String toString() { // TODO Auto-generated method stub
	 * return
	 * "Date: "+date+", Message: "+message+", Author: "+author+", Revision: "
	 * +revision; }
	 */

}
