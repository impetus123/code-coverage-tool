import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import db.DBConnection;

public class GITCodeCoverage implements CodeCoverage {

	private static String gitSaveLogQuery = "insert into git_log (AUTHOR,DATE,REVISION,MESSAGE,PATH) values (?,?,?,?,?)";
	private String gitGetLogQuery = "select * from git_log where message like ?";
	private PreparedStatement psmt = null;
	private Connection conn = DBConnection.connect();

	@Override
	public void createBlame(String pathToSrc, String userStory, Connection conn)
			throws SQLException {

		ResultSet resultSet = null;
		psmt = this.conn.prepareStatement(gitGetLogQuery);
		// psmt.setString(1, "%"+userStory+"%");
		psmt.setString(1, "%cent%");
		resultSet = psmt.executeQuery();
		Map filesForBlame = new HashMap();
		String fileNames = "";
		String files[] = null;
		int i = 0;
		String revision = "";
		while (resultSet.next()) {

			fileNames = resultSet.getString("PATH").replace("[", "")
					.replace("]", "");
			revision = resultSet.getString("REVISION");
			files = fileNames.split(", ");
			try {
				insertIntoBlameDB(files, revision);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void insertIntoBlameDB(String[] files, String revision)
			throws IOException {

		// String
		// commandToBeExecuted="git blame  '"+files[i]+"' | grep "+revision+" | awk '{print $7}'";
		// String
		// commandToBeExecuted="git --git-dir D:\\code_coverage\\.git blame src\\RunCodeCoverage.java | grep 'e4ecfbe'";
		// git blame --git-dir D:/code_coverage/.git src/RunCodeCoverage.java|
		// grep 59e3fdc | gawk '{print $7}'
		String commandToBeExecuted = "git --git-dir D:\\code_coverage\\.git blame  'src/RunCodeCoverage.java' ";// |
																												// grep
																												// e4ecfbe
																												// |
																												// awk
																												// \"{print $7}\"";//"git --git-dir D:\\code_coverage\\.git blame  'src/RunCodeCoverage.java' | grep e4ecfbe ";//|
																												// gawk
																												// '{print
																												// $7}'";
		String temp = "";
		File file = new File("C:/Program Files (x86)/Git/bin");
		System.out.println(commandToBeExecuted);
		// Process p = Runtime.getRuntime().exec(commandToBeExecuted);
		// Process p = Runtime.getRuntime().exec(new String[]{"cmd", "/c",
		// commandToBeExecuted});
		Process p = Runtime.getRuntime().exec(commandToBeExecuted, null, file);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		BufferedReader stdError = new BufferedReader(new InputStreamReader(
				p.getErrorStream()));
		while ((temp = stdError.readLine()) != null) {
			System.out.println(temp);
		}

	}

	public static void main(String args[]) {
		try {
			new GITCodeCoverage().insertIntoBlameDB(null, null);
			;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
	}

	@Override
	public void createLog(String pathToSrc) {

		String s = null, fileName = "./xml/gitLog.txt";
		String userStoryMessage = "Init";
		if (pathToSrc != null
				&& !(pathToSrc.endsWith("/") || pathToSrc.endsWith("\\"))) {
			pathToSrc = pathToSrc.concat("/");
		}

		String commandToBeExecuted = "git --git-dir D:/code_coverage/.git log --pretty=format:\"revision=\"%h\",%nauthor=\"%an\",%nmessage=\"%s\",%ndate=\"%cd\",\" --grep="
				+ userStoryMessage + " ";
		// String
		// commandToBeExecuted="git --git-dir "+pathToSrc+" log --pretty=format:\"revision=\"%h\",%nauthor=\"%an\",%nmessage=\"%s\",%ndate=\"%cd\",\" --grep="+userStoryMessage+" ";
		try {

			System.out.println(commandToBeExecuted);
			Process p = Runtime.getRuntime().exec(commandToBeExecuted);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			File file = new File(fileName);
			FileOutputStream is = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(is);
			Writer w = new BufferedWriter(osw);
			while ((s = stdInput.readLine()) != null) {
				w.write(s + "\n");
			}
			w.close();
			saveLogInDB(fileName);
			System.out.println("Logs Saved Successfully.....\n\n");
		} catch (Exception e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private void saveLogInDB(String fileName) throws SQLException {

		BufferedReader br = null;

		psmt = conn.prepareStatement(gitSaveLogQuery);
		ArrayList<GitLogInfo> logList = new ArrayList<GitLogInfo>();
		List list = new ArrayList<>();
		GitLogInfo logInfo = new GitLogInfo();
		try {

			String sCurrentLine;
			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) {

				if (sCurrentLine.startsWith("author")) {
					logInfo.setAuthor(sCurrentLine.substring(
							sCurrentLine.indexOf("=") + 1,
							sCurrentLine.indexOf(",")));
				} else if (sCurrentLine.startsWith("message")) {
					logInfo.setMessage(sCurrentLine.substring(
							sCurrentLine.indexOf("=") + 1,
							sCurrentLine.indexOf(",")));
				} else if (sCurrentLine.startsWith("date")) {
					logInfo.setDate(sCurrentLine.substring(
							sCurrentLine.indexOf("=") + 1,
							sCurrentLine.indexOf(",")));
				} else if (sCurrentLine.startsWith("revision")) {
					list.add(sCurrentLine.substring(
							sCurrentLine.indexOf("=") + 1,
							sCurrentLine.indexOf(",")));
					logInfo.setRevision(sCurrentLine.substring(
							sCurrentLine.indexOf("=") + 1,
							sCurrentLine.indexOf(",")));
				}
				if (logInfo.getAuthor() == null || logInfo.getMessage() == null
						|| logInfo.getRevision() == null
						|| logInfo.getDate() == null) {
					continue;
				}
				logList.add(logInfo);
				logInfo = new GitLogInfo();
			}
			System.out.println(list.size());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}

		List files = new ArrayList();
		String s = "";
		int count = 0;
		Map<Object, Object> filesMap = new HashMap<Object, Object>();
		while (count < list.size()) {

			Process p = null;
			;
			try {
				p = Runtime.getRuntime().exec(
						"git --git-dir D:/code_coverage/.git show --pretty=\"format:\" --name-only "
								+ list.get(count));
			} catch (IOException e) {
				e.printStackTrace();
			}

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
			try {
				while ((s = stdInput.readLine()) != null) {
					if (s.length() > 0 && s.endsWith(".java")) {
						files.add(s);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			filesMap.put(list.get(count), files);
			files = new ArrayList();
			count++;
		}

		Iterator<GitLogInfo> itr = logList.iterator();
		GitLogInfo logInfo1 = null;
		while (itr.hasNext()) {
			logInfo1 = itr.next();
			/*
			 * System.out.println(logInfo1.getAuthor());
			 * System.out.println(logInfo1.getDate());
			 * System.out.println(logInfo1.getRevision());
			 * System.out.println(logInfo1.getMessage());
			 */

			psmt.setString(1, logInfo1.getAuthor());
			psmt.setString(2, logInfo1.getDate());
			psmt.setString(3, logInfo1.getRevision());
			psmt.setString(4, logInfo1.getMessage());
			psmt.setString(5, filesMap.get(logInfo1.getRevision()).toString());

			psmt.execute();

		}

		psmt.close();
		conn.close();

	}
}
